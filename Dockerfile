# Nginx Build Image
FROM nginx:latest as builder

RUN apt-get update
RUN apt-get install -y libpcre3-dev libssl-dev zlib1g-dev wget build-essential

WORKDIR /tmp
RUN wget "https://www.openssl.org/source/openssl-1.1.1b.tar.gz"
RUN tar xzf openssl-1.1.1b.tar.gz
WORKDIR /tmp/openssl-1.1.1b
RUN ./config
#RUN make test

# Final docker image
FROM nginx:latest

MAINTAINER Jamie Curnow <jc@jc21.com>
LABEL maintainer="Jamie Curnow <jc@jc21.com>"

RUN apt-get update \
  && apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -y build-essential \
  && apt-get clean

# Openssl custom
COPY --from=builder /tmp/openssl-1.1.1b /tmp/openssl-1.1.1b
WORKDIR /tmp/openssl-1.1.1b
RUN make install \
    && ldconfig \
    && openssl version \
    && rm -rf /tmp/openssl-1.1.1b

RUN apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -y curl gnupg dirmngr apt-transport-https wget ca-certificates git \
  && apt-key adv --fetch-keys https://dl.yarnpkg.com/debian/pubkey.gpg \
  && echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list \
  && apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -y \
        inetutils-ping \
        letsencrypt \
        apache2-utils \
        yarn \
  && apt-get clean

# NodeJS
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt-get install -y nodejs \
    && apt-get clean \
    && npm install -g gulp





MAINTAINER Jamie Curnow <jc@jc21.com>
LABEL maintainer="Jamie Curnow <jc@jc21.com>"

ENV SUPPRESS_NO_CONFIG_WARNING=1
ENV S6_FIX_ATTRS_HIDDEN=1
RUN echo "fs.file-max = 65535" > /etc/sysctl.conf

# Nginx, Node and required packages should already be installed from the base image

# root filesystem
COPY rootfs /

# s6 overlay
RUN curl -L -o /tmp/s6-overlay-amd64.tar.gz "https://github.com/just-containers/s6-overlay/releases/download/v1.21.4.0/s6-overlay-amd64.tar.gz" \
    && tar xzf /tmp/s6-overlay-amd64.tar.gz -C /

WORKDIR /
#RUN npm run build

# App
ENV NODE_ENV=production

ADD package.json        /app/package.json
RUN npm run build
ADD dist                /app/dist
ADD node_modules        /app/node_modules
ADD src/backend         /app/src/backend
ADD knexfile.js         /app/knexfile.js

# Volumes
VOLUME [ "/data", "/etc/letsencrypt" ]
CMD [ "/init" ]

# Ports
EXPOSE 80
EXPOSE 81
EXPOSE 443
EXPOSE 9876

HEALTHCHECK --interval=15s --timeout=3s CMD curl -f http://localhost:9876/health || exit 1